# Rockem Sockets, A basic FTP program

Rockem Sockets is a program that acts like an FTP (File Transfer Protocol) program. It consists of a server and a client. The client program can send and retrieve files to and from the server and can also display the active working directory where the server program is running.

This program was a project for CS333: Intro to Operating Systems at Portland State University Winter Term 2024. The program was made with the lectures and help of Professor Chaney (rchaney@pdx.edu) and from the class textbook, "The Linux Programming Interface" by Michael Kerrisk. A [fork](https://gitlab.cecs.pdx.edu/bpl4/linux-ftp-bpl-4) has been made to add more features and basic security to this program.. See more below.

## Features

- Send files from the client to the server using the `put` command.
- Receive files from the server to the client using the `get` command.
- Display the server's working directory using the `dir` command.
- See list of commands and usage with the -h option.

## Getting Started

Before using, you must port forward the desired ports for TCP on the system that will be running the server program. You must also configure the firewall to allow incoming and outgoing connections and data. If using a virtual machine, you must also enable port forwarding in the virtual machine's settings.

To get started, you can run:
```bash
./rockem_server -h
```
and
```bash
./rockem_client -h
```

### Basic Server Setup

To run the server, execute the following command:

```bash
./rockem_server -p <port_number>
```

The server will run indefinitely until the user enters `exit`. It will actively wait for connections from any client. If the user does not specify a port, the default port, 10001, will be used. After running the server, you can check to see if the server can be connected by visiting https://canyouseeme.org and enter the port number that was used to run with the server.

### Basic Client Setup

To run the client program, use the following command:

```bash
./rockem_client -i <public_IPv4_address> -p <port_number> -c <command> ...
```
The IPv4 address must be a public address of the system that is running the server. You can find the server's address by visiting https://www.whatismyip.com on the network where the server is running.

The following commands are supported for the `-c` option:

- `dir` - Displays the server's working directory.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -c dir
    ```

- `put` - Sends files from the client to the server.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -c put file1.txt file2.txt ...
    ```

- `get` - Receives files from the server to the client.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -c get file1.txt file2.txt ...
    ```

### Known Issues
- There will be a chance that if you get/put a file(s) more than twice, you may create a "holey" file. This occurs whenever the file that we are putting or getting already exists in the target directory. We can fix this by deleting that file from the target directory before performing the put/get.
- There is a chance that the server will write an empty file when the client performs a put operation. This issue also seems to be present when the client is running linux on a virtual machine (like VirtualBox). The fix is to enable port forwarding in the virtual machine's network settings. For VirtualBox, after selecting the desired Linux image, go to it's Settings>Network>Advanced>Port Forwarding and add a rule to allow a port from which the server is using.
- If we put or get a file that does not exist, the server or client will create an empty file with the name anyways. This is normal and a simple fix would be to fork a process and exec() rm on that file. 


