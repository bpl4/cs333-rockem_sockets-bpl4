// Brian Le
// R. Chaney: rchaney@pdx.edu
// This FTP Rockem Socket program was written with the help of Professor Chaney and information from "The Linux Programming Interface" textbook
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/uio.h>
#include <unistd.h>
#include <pthread.h>

#include "rockem_hdr.h"

static unsigned short is_verbose = 0;
static unsigned sleep_flag = 0;

//static char ip_addr[50] = "131.252.208.23"; this is the default ip address of PSU.
static char ip_addr[50] = {"\0"};
static short ip_port = DEFAULT_SERV_PORT;

int get_socket(char *, int);
void get_file(char *);
void put_file(char *);
void *thread_get(void *);
void *thread_put(void *);
void list_dir(void);

int
main(int argc, char *argv[])
{
    cmd_t cmd;
    int opt;
    int i = 0;
    int j = 0;
    pthread_t* threads = NULL;

    memset(&cmd, 0, sizeof(cmd_t));
    while ((opt = getopt(argc, argv, CLIENT_OPTIONS)) != -1) {
        switch (opt) {
        case 'i':
            // copy optarg into the ip_addr
	    strcpy(ip_addr, optarg);
            break;
        case 'p':
            // CONVERT and assign optarg to ip_port
	    sscanf(optarg, "%hd", &ip_port);
            break;
        case 'c':
            // copy optarg into data member cmd.cmd
	    strcpy(cmd.cmd, optarg);
            break;
        case 'v':
            is_verbose++;
            break;
        case 'u':
            // add 1000 to sleep_flag
	    sleep_flag += 1000;
            break;
        case 'h':
            fprintf(stderr, "%s ...\n\tOptions: %s\n"
                    , argv[0], CLIENT_OPTIONS);
            fprintf(stderr, "\t-i str\t\tIPv4 address of the server (default %s)\n"
                    , ip_addr);
            fprintf(stderr, "\t-p #\t\tport on which the server will listen (default %hd)\n"
                    , DEFAULT_SERV_PORT);
            fprintf(stderr, "\t-c str\t\tcommand to run (one of %s, %s, or %s)\n"
                    , CMD_GET, CMD_PUT, CMD_DIR);
            fprintf(stderr, "\t-u\t\tnumber of thousands of microseconds the client will sleep between read/write calls (default %d)\n"
                    , 0);
            fprintf(stderr, "\t-v\t\tenable verbose output. Can occur more than once to increase output\n");
            fprintf(stderr, "\t-h\t\tshow this rather lame help message\n");
            exit(EXIT_SUCCESS);
            break;
        default:
            fprintf(stderr, "*** Oops, something strange happened <%s> ***\n", argv[0]);
            break;
        }
    }

    if (strlen(ip_addr) == 0) {
	    fprintf(stderr, "Must provide a public IPv4 address\n");
	    exit(EXIT_FAILURE);
    }

    threads = malloc(sizeof(pthread_t) * (argc - optind));

    if (is_verbose)
    	fprintf(stderr, "There are %d files given.\n", ((argc - optind)));

    if (is_verbose) {
        fprintf(stderr, "Command to server: <%s> %d\n"
                , cmd.cmd, __LINE__);
    }
    if (strcmp(cmd.cmd, CMD_GET) == 0) {
        // process the files left on the command line, creating a threas for
        // each file to connect to the server
        for (i = 0; i < argc - optind; i++) {
            // create threads
            // pass the file name as the ONE parameter to the thread function
	    pthread_create(&(threads[i]), NULL, thread_get, argv[optind + i]);
        }
    }
    else if (strcmp(cmd.cmd, CMD_PUT) == 0) {
        // process the files left on the command line, creating a threas for
        // each file to connect to the server
        for (j = 0; j < argc - optind; j++) {
            // create threads
            // pass the file name as the ONE parameter to the thread function
	    pthread_create(&(threads[j]), NULL, thread_put, argv[optind + j]);
        }
    }
    else if (strcmp(cmd.cmd, CMD_DIR) == 0) {
        list_dir();
    }
    else {
        fprintf(stderr, "ERROR: unknown command >%s< %d\n", cmd.cmd, __LINE__);
	free(threads);
        exit(EXIT_FAILURE);
    }

    free(threads);

    pthread_exit(NULL);
}

int
get_socket(char * addr, int port)
{
    // configure and create a new socket for the connection to the server
    int sockfd;
    struct sockaddr_in servaddr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&servaddr, 0, sizeof(servaddr));

    // more stuff in here
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    inet_pton(AF_INET, addr, &servaddr.sin_addr.s_addr);

    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0)
    {
	    perror("could not connect");
	    exit(EXIT_FAILURE);
    }

    return(sockfd);
}

// get one file
void
get_file(char *file_name)
{
    cmd_t cmd;
    int sockfd;
    int fd;
    ssize_t bytes_read;
    char buffer[MAXLINE];

    strcpy(cmd.cmd, CMD_GET);
    if (is_verbose) {
        fprintf(stderr, "next file: <%s> %d\n", file_name, __LINE__);
    }
    strcpy(cmd.name, file_name);
    if (is_verbose) {
        fprintf(stderr, "get from server: %s %s %d\n", cmd.cmd, cmd.name, __LINE__);
    }

    // get the new socket to the server (get_socket(...)
    sockfd = get_socket(ip_addr, ip_port);

    // write the command to the socket
    write(sockfd, &cmd, sizeof(cmd)); 

    // open the file to write
    fd = open(file_name, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR, S_IWUSR);
    chmod(file_name, S_IRUSR | S_IWUSR);

    // loop reading from the socket, writing to the file
    //   until the socket read is zero
    while((bytes_read = read(sockfd, buffer, MAXLINE)) > 0)
    {
	    write(fd, buffer, bytes_read);
    	    if (sleep_flag > 0)
		    usleep(sleep_flag);
    }

    // close the file
    close(fd);
    // close the socket
    close(sockfd);
}

void
put_file(char *file_name)
{
    cmd_t cmd;
    int sockfd;
    int fd;
    ssize_t bytes_read;
    char buffer[MAXLINE];

    strcpy(cmd.cmd, CMD_PUT);
    if (is_verbose) {
        fprintf(stderr, "next file: <%s> %d\n", file_name, __LINE__);
    }
    strcpy(cmd.name, file_name);
    if (is_verbose) {
        fprintf(stderr, "put to server: %s %s %d\n", cmd.cmd, cmd.name, __LINE__);
    }

    // get the new socket to the server (get_socket(...)
    sockfd = get_socket(ip_addr, ip_port); 
    if (sockfd < 0) {
	    perror("Failed to get socket\n");
    }
    //cmd.sock = sockfd;

    // write the command to the socket
    write(sockfd, &cmd, sizeof(cmd_t));

    // open the file for read
    fd = open(file_name, O_RDONLY);

    // loop reading from the file, writing to the socket
    //   until file read is zero
    memset(buffer, 0, sizeof(buffer));
    while((bytes_read = read(fd, buffer, MAXLINE)) > 0)
    {
	    write(sockfd, buffer, bytes_read);
	    if (sleep_flag > 0)
		    usleep(sleep_flag);
    }

    // close the file
    close(fd);
    // close the socket
    close(sockfd);
}

void
list_dir(void)
{
    cmd_t cmd;
    int sockfd;
    ssize_t bytes_read;
    char buffer[MAXLINE];

    if (is_verbose)
	    printf("dir from server: %s \n", cmd.cmd);

    // get the new socket to the server (get_socket(...)
    sockfd = get_socket(ip_addr, ip_port);

    strcpy(cmd.cmd, CMD_DIR);
    // write the command to the socket

    write(sockfd, &(cmd), sizeof(cmd));

    // loop reading from the socket, writing to the file
    //   until the socket read is zero
    while((bytes_read = read(sockfd, buffer, MAXLINE)) > 0)
    {
	    write(STDOUT_FILENO, buffer, bytes_read);
    }

    // close the socket
    close(sockfd);
}

void *
thread_get(void *info)
{
    char *file_name = (char *) info;

    // detach this thread 'man pthread_detach' Look at the EXMAPLES
    pthread_detach(pthread_self());

    // process one file
    get_file(file_name);

    pthread_exit(NULL);
}

void *
thread_put(void *info)
{
    char *file_name = (char *) info;

    // detach this thread 'man pthread_detach' Look at the EXMAPLES
    pthread_detach(pthread_self());

    // process one file
    put_file(file_name);

    pthread_exit(NULL);
}
